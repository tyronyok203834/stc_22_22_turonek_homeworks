import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static int toInt(int[] number) {

        int result = 0;

        for (int i = 0; i <= number.length - 1; i++) {
            result = 10 * result + number[i];
        }
        return result;
    }

    public static int sumInRange(int[] a, int from, int to) {

        int sum = 0;

        if (from > to || from < 0 || to >= a.length) {
            System.out.print("\nИнтервал задан неверно: " + -1);
        } else {
            System.out.print("\nСумма чисел интервала: ");
            for (int i = from; i <= to; i++) {
                sum += a[i];
            }
        }
        return sum;
    }

    public static void printEvenNumbers(int[] Array) {

        for (int i = 0; i <= Array.length - 1; i++) {
            if (Array[i] % 2 == 0) {
                System.out.print(" " + Array[i]);
            }
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество элементов массива: ");
        int arr = scanner.nextInt();

        if (arr < 2) {
            System.out.print("В массиве должно быть не менее 2 элементов, для их суммы!");
            System.exit(0);
        }

        int[] array = new int[arr];

        System.out.print("\nВведите элементы массива в строку: ");
        for (int i = 0; i < arr; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.print("\nВведенные в массив элементы: " + Arrays.toString(array));

        System.out.print("\n\nВведите значение левой границы (индекс): ");
        int index1 = scanner.nextInt();
        System.out.print("\nВведите значение правой границы (индекс): ");
        int index2 = scanner.nextInt();

        int sum = sumInRange(array, index1, index2);

        System.out.println(sum);

        System.out.print("\nЧетные элементы массива:");
        printEvenNumbers(array);

        int result = toInt(array);

        System.out.println("\n\nResult == " + result);
    }
}
