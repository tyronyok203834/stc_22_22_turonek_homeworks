package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.dto.DepartmentForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.DepartmentService;

@RequiredArgsConstructor
@Controller
public class DepartmentsController {

    private final DepartmentService departmentService;

    @PostMapping("/departments/{department-id}/students")
    public String addStudentToDepartment(@PathVariable("department-id") Long departmentId,
                                         @RequestParam("student-id") Long studentId) {
        departmentService.addStudentToDepartment(departmentId, studentId);
        return "redirect:/departments/" + departmentId;
    }

    @GetMapping("/departments/{department-id}")
    public String getDepartmentPage(@PathVariable("department-id") Long departmentId, Model model) {
        model.addAttribute("department", departmentService.getDepartment(departmentId));
        model.addAttribute("notInDepartmentStudents", departmentService.getNotInDepartmentStudents(departmentId));
        model.addAttribute("inDepartmentStudents", departmentService.getInDepartmentStudents(departmentId));
        model.addAttribute("notInDepartmentBooks", departmentService.getNotInDepartmentBooks());
        model.addAttribute("inDepartmentBooks", departmentService.getInDepartmentBooks(departmentId));
        return "department_page";
    }

    @GetMapping("/departments")
    public String getDepartmentsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("departments", departmentService.getAllDepartments());
        return "departments_page";
    }

    @PostMapping("/departments")
    public String addDepartment(DepartmentForm department) {
        departmentService.addDepartment(department);
        return "redirect:/departments";
    }

    @GetMapping("/departments/{department-id}/delete")
    public String deleteDepartment(@PathVariable("department-id") Long departmentId) {
        departmentService.deleteDepartment(departmentId);
        return "redirect:/departments/";
    }

    @PostMapping("/departments/{department-id}/update")
    public String updateDepartment(@PathVariable("department-id") Long departmentId, DepartmentForm department) {
        departmentService.updateDepartment(departmentId, department);
        return "redirect:/departments/" + departmentId;
    }

    @PostMapping("/departments/{department-id}/book")
    public String addLessonsToDepartment(@PathVariable("department-id") Long departmentId,
                                         @RequestParam("book-id") Long bookId) {
        departmentService.addLessonToDepartment(departmentId, bookId);
        return "redirect:/departments/" + departmentId;
    }
}

