package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.ec.dto.BookForm;
import ru.inno.ec.services.BooksService;


@RequiredArgsConstructor
@Controller
public class BooksController {

    private final BooksService booksService;

    @GetMapping("/books")
    public String getBooksPage(Model model) {
        model.addAttribute("books", booksService.getAllBooks());
        return "books_page";
    }

    @GetMapping("/books/{book-id}")
    public String getBookPage(@PathVariable("book-id") Long bookId, Model model) {
        model.addAttribute("book", booksService.getBook(bookId));
        return "book_page";
    }

    @PostMapping("/books")
    public String addBook(BookForm book) {
        booksService.addBook(book);
        return "redirect:/books";
    }

    @GetMapping("/books/{book-id}/delete")
    public String deleteBook(@PathVariable("book-id") Long bookId) {
        booksService.deleteBook(bookId);
        return "redirect:/books/";
    }

    @PostMapping("/books/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, BookForm book) {
        booksService.updateBook(bookId, book);
        return "redirect:/books/" + bookId;
    }
}


