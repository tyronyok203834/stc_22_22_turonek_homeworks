package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.DepartmentForm;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.Department;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.DepartmentsRepository;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.DepartmentService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentsRepository departmentsRepository;
    private final UsersRepository usersRepository;
    private final BooksRepository booksRepository;

    @Override
    public List<Department> getAllDepartments() {
        return departmentsRepository.findAllByStateNot(Department.State.DELETED);

    }


    @Override
    public void addDepartment(DepartmentForm course) {
        Department newDepartment = Department.builder()
                .title(course.getTitle())
                .startWork(course.getStartWork())
                .finishWork(course.getFinishWork())
                .state(Department.State.CONFIRMED)
                .description(course.getDescription())
                .build();
        departmentsRepository.save(newDepartment);
    }

    @Override
    public void addStudentToDepartment(Long courseId, Long studentId) {
        Department department = departmentsRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getDepartment().add(department);

        usersRepository.save(student);
    }

    @Override
    public Department getDepartment(Long courseId) {

        return departmentsRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInDepartmentStudents(Long courseId) {
        Department department = departmentsRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByDepartmentNotContains(department);
    }

    @Override
    public List<User> getInDepartmentStudents(Long courseId) {
        Department department = departmentsRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByDepartmentContains(department);
    }

    @Override
    public void deleteDepartment(Long courseId) {
        Department departmentForDelete = departmentsRepository.findById(courseId).orElseThrow();
        departmentForDelete.setState(Department.State.DELETED);

        departmentsRepository.save(departmentForDelete);


    }

    @Override
    public void updateDepartment(Long courseId, DepartmentForm updateData) {
        Department departmentForUpdate = departmentsRepository.findById(courseId).orElseThrow();
        departmentForUpdate.setStartWork(updateData.getStartWork());
        departmentForUpdate.setFinishWork(updateData.getFinishWork());
        departmentForUpdate.setDescription(updateData.getDescription());
        departmentsRepository.save(departmentForUpdate);
    }

    @Override
    public void addLessonToDepartment(Long courseId, Long lessonId) {
        Department department = departmentsRepository.findById(courseId).orElseThrow();
        Book book = booksRepository.findById(lessonId).orElseThrow();

        book.setDepartment(department);
        booksRepository.save(book);

    }

    @Override
    public List<Book> getNotInDepartmentBooks() {
        return booksRepository.findAllByDepartmentNull();
    }


    @Override
    public List<Book> getInDepartmentBooks(Long courseId) {
        Department department = departmentsRepository.findById(courseId).orElseThrow();
        return booksRepository.findAllByDepartment(department);
    }
}





