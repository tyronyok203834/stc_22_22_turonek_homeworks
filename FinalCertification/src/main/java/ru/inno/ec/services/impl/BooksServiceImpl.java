package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.BookForm;
import ru.inno.ec.models.Book;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.services.BooksService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }

    @Override
    public Book getBook(Long bookId) {
        return booksRepository.findById(bookId).orElseThrow();
    }

    @Override
    public void addBook(BookForm book) {
        Book newBook = Book.builder()
                .author(book.getAuthor())
                .startTime(book.getStartTime())
                .finishTime(book.getFinishTime())
                .state(Book.State.CONFIRMED)
                .summary(book.getSummary())
                .build();
        booksRepository.save(newBook);
    }

    @Override
    public void deleteBook(Long bookId) {
        Book bookForDelete = booksRepository.findById(bookId).orElseThrow();
        bookForDelete.setState(Book.State.DELETED);

        booksRepository.save(bookForDelete);
    }

    @Override
    public void updateBook(Long bookId, BookForm updateData) {
        Book bookForUpdate = booksRepository.findById(bookId).orElseThrow();
        bookForUpdate.setStartTime(updateData.getStartTime());
        bookForUpdate.setFinishTime(updateData.getFinishTime());
        bookForUpdate.setSummary(updateData.getSummary());
        bookForUpdate.setAuthor(updateData.getAuthor());
        booksRepository.save(bookForUpdate);
    }
}


