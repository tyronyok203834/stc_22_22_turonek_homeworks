package ru.inno.ec.services;

import ru.inno.ec.dto.DepartmentForm;
import ru.inno.ec.models.Department;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.User;

import java.util.List;

public interface DepartmentService {
    List<Department> getAllDepartments();

    void addDepartment(DepartmentForm department);

    void addStudentToDepartment(Long departmentId, Long studentId);

    Department getDepartment(Long departmentId);

    List<User> getNotInDepartmentStudents(Long departmentId);

    List<User> getInDepartmentStudents(Long departmentId);

    void deleteDepartment(Long departmentId);

    void updateDepartment(Long departmentId, DepartmentForm department);

    void addLessonToDepartment(Long departmentId, Long bookId);

    List<Book> getNotInDepartmentBooks();

    List<Book> getInDepartmentBooks(Long departmentId);
}





