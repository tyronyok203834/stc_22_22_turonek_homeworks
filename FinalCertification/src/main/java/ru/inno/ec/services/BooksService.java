package ru.inno.ec.services;

import ru.inno.ec.dto.BookForm;
import ru.inno.ec.models.Book;

import java.util.List;

public interface BooksService {

    List<Book> getAllBooks();

    Book getBook(Long bookId);

    void addBook(BookForm book);

    void deleteBook(Long bookId);

    void updateBook(Long bookId, BookForm book);
}
