package ru.inno.ec.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"books", "students"})
@Entity
@Table(name = "department")
public class Department {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;


    @Column(length = 500)
    private String description;
    @Column(name = "start_work")
    private LocalTime startWork;
    @Column(name = "finish_work")
    private LocalTime finishWork;

    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<Book> books;

    @ManyToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<User> students;

    @Enumerated(value = EnumType.STRING)
    private State state;
}






