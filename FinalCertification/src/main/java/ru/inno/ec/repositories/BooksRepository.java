package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Department;
import ru.inno.ec.models.Book;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Long> {

    List<Book> findAllByStateNot(Book.State state);

    List<Book> findAllByDepartmentNull();

    List<Book> findAllByDepartment(Department department);
}



