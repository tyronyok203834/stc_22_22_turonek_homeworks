package ru.inno.ec.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DepartmentForm {

    private String title;

    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime startWork;

    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime finishWork;

    private String description;
}



