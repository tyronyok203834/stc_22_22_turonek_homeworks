public class Main {

    public static void completeAllTasks(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }

    public static void main(String[] args) {

        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(0, 10);

        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(5, 15);

        Task[] tasks = {evenNumbersPrintTask, oddNumbersPrintTask};
        completeAllTasks(tasks);

    }
}