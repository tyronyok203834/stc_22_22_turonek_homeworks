public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;

    private int count;

    private static class Node<T> {
        T value;
        Node<T> next;

        Node<T> previous;

        public Node(T value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        Node<T> previousNode = null;
        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
            previousNode = last;
        }
        this.last = newNode;
        last.previous = previousNode;
        count++;
    }

    @Override
    public void remove(T element) {
        Node<T> current = first;
        Node<T> previous;
        Node<T> firstAddressOfElement;
        while (current != null) {
            if (current.value.equals(element)) {
                if (current.previous == null) {
                    firstAddressOfElement = current;
                    first = firstAddressOfElement.next;
                    first.previous = null;
                    count--;
                    break;
                }
                firstAddressOfElement = current;
                previous = firstAddressOfElement.previous;
                previous.next = firstAddressOfElement.next;
                count--;
                break;
            }
            current = current.next;
        }
    }

    @Override
    public boolean contains(T element) {
        Node<T> current = this.first;
        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index > 0 && index < count) {
            Node<T> current = first;
            Node<T> previous;
            Node<T> elementAddress;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            elementAddress = current;
            previous = elementAddress.previous;
            previous.next = elementAddress.next;
            count--;
        } else if (index == 0) {
            Node<T> next = first.next;
            first = next;
            first.previous = null;
            count--;
        }
    }

    @Override
    public String toString() {
        StringBuilder array = new StringBuilder();
        String result;
        Node<T> current = first;
        for (int i = 0; i < count; i++) {
            array.append(current.value).append(" ");
            current = current.next;
        }
        result = String.valueOf(array);
        return result;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            Node<T> current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;
    }
}

