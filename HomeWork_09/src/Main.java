public class Main {

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("World!");
        stringList.add("C++");
        stringList.add("Bye!");
        stringList.add("Red");
        stringList.add("Hello!");
        stringList.add("PHP");

        System.out.println(stringList.get(3));
        System.out.println(stringList.contains("World!"));
        System.out.println(stringList.contains("Car"));
        System.out.println(stringList.size());

        stringList.remove("Bye!");
        System.out.println(stringList);

        stringList.removeAt(0);
        System.out.println(stringList);

    }
}
