
public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpI("Products.txt");
        Product milk = productsRepository.productFindById(1);
        System.out.println(milk);
        System.out.println(productsRepository.findAllByTitleLike("ОЛО"));
        milk.setCost(304.5);
        milk.setQuantity(50);
        productsRepository.update(milk);
        System.out.println(milk);
    }
}