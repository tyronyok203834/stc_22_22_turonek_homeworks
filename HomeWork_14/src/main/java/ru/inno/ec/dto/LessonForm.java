package ru.inno.ec.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LessonForm {

    private String name;

    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime startTime;

    @DateTimeFormat(pattern = "HH:MM")
    private LocalTime finishTime;

    private String summary;

}


