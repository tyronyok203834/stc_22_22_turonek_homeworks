package ru.inno.ec.services;

import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesService {
    List<Course> getAllCourses();

    void addCourse(CourseForm course);

    void addStudentToCourse(Long courseId, Long studentId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    void deleteCourse(Long courseId);

    void updateCourse(Long courseId, CourseForm course);

    void addLessonToCourse(Long courseId, Long lessonId);

    List<Lesson> getNotInCourseLessons(Long courseId);

    List<Lesson> getInCourseLessons(Long courseId);
}




