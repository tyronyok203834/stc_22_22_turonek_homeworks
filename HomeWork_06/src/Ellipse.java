public class Ellipse extends Circle {

    private final int bigRadius;

    public Ellipse(int smallRadius, int bigRadius, int x, int y) {
        super(smallRadius, x, y);
        this.bigRadius = bigRadius;
    }

    @Override
    public String toString() {
        return "Ellipse{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public double area() {
        return smallRadius * bigRadius * Math.PI;
    }

    @Override
    public double perimeter() {
        return 4 * (Math.PI * smallRadius * bigRadius + (bigRadius - smallRadius)) / (smallRadius + bigRadius);
    }
}

