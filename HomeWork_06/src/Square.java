public class Square extends Figure {

    protected int a;

    public Square(int a, int x, int y) {
        super(x, y);
        this.a = a;
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public double area() {
        return a * a;
    }

    @Override
    public double perimeter() {
        return 4 * a;
    }
}

