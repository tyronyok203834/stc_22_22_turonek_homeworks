public class Main {

    public static void main(String[] args) {

        Square square = new Square(5, 0, 0);
        Rectangle rectangle = new Rectangle(5, 6, 0, 0);
        Circle circle = new Circle(7, 0, 0);
        Ellipse ellipse = new Ellipse(7, 11, 0, 0);

        Figure[] figures = new Figure[]{circle, rectangle, square, ellipse};
        square.move(-3, 5);

        for (Figure figure : figures) {
            System.out.println("area = " + figure.area() + "     " + figure.toString());
            System.out.println("perimeter = " + figure.perimeter());
        }
    }
}



