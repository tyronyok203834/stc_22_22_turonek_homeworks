public class Rectangle extends Square {

    private final int b;

    public Rectangle(int a, int b, int x, int y) {
        super(a, x, y);
        this.b = b;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public double area() {
        return a * b;
    }

    @Override
    public double perimeter() {
        return 2 * (a + b);
    }
}
