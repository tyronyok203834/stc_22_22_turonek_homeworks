public class Circle extends Figure {
    protected int smallRadius;

    public Circle(int radius, int x, int y) {
        super(x, y);
        this.smallRadius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public double area() {
        return Math.PI * smallRadius * smallRadius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * smallRadius;
    }
}
