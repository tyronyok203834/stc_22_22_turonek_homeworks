
public abstract class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int move_X, int move_Y) {
        this.x += move_X;
        this.y += move_Y;
    }

    @Override
    public String toString() {
        return "Figure{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public abstract double area();

    public abstract double perimeter();
}

