public class ATM {
    private int restOfMoneyATM;
    private final int MAX_ALLOWED_AMOUNT_TO_WITHDRAW;
    private final int MAX_AMOUNT_OF_MONEY_IN_AN_ATM;
    private int numberOfOperations;

    public ATM(int MAX_ALLOWED_AMOUNT_TO_WITHDRAW, int MAX_AMOUNT_OF_MONEY_IN_AN_ATM, int numberOfOperations, int restOfMoneyATM) {
        this.MAX_ALLOWED_AMOUNT_TO_WITHDRAW = MAX_ALLOWED_AMOUNT_TO_WITHDRAW;
        this.MAX_AMOUNT_OF_MONEY_IN_AN_ATM = MAX_AMOUNT_OF_MONEY_IN_AN_ATM;
        this.numberOfOperations = numberOfOperations;
        this.restOfMoneyATM = restOfMoneyATM;
    }

    public int giveMoney(int money) {
        this.numberOfOperations++;
        if (money <= this.MAX_ALLOWED_AMOUNT_TO_WITHDRAW && money <= this.restOfMoneyATM) {
            this.restOfMoneyATM -= money;
            return money;
        } else {
            return 0;
        }
    }

    public int putMoney(int money) {
        this.numberOfOperations++;
        if (money > (this.MAX_AMOUNT_OF_MONEY_IN_AN_ATM - this.restOfMoneyATM)) {
            money = money - (this.MAX_AMOUNT_OF_MONEY_IN_AN_ATM - this.restOfMoneyATM);
            this.restOfMoneyATM = this.MAX_AMOUNT_OF_MONEY_IN_AN_ATM;
            return money;
        } else {
            this.restOfMoneyATM += money;
            return 0;
        }
    }

    public int getRestOfMoneyATM() {
        return this.restOfMoneyATM;
    }

    public int getNumberOfOperations() {
        return this.numberOfOperations;
    }

    public int getMAX_ALLOWED_AMOUNT_TO_WITHDRAW() {
        return MAX_ALLOWED_AMOUNT_TO_WITHDRAW;
    }
}

