import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int money;

        ATM machine = new ATM(50000, 100000, 0, 80000);

        System.out.println("*******************************");
        System.out.println("********** БАНКОМАТ ***********");
        System.out.println("*******************************");
        System.out.println("Введите 1 - операция по снятию.");
        System.out.println("Введите 2 - операция по вкладу.");
        System.out.println("Введите 3 - завершение работы");
        System.out.println("*******************************");

        while (true) {

            System.out.print("Введите номер операции: ");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1 -> {
                    System.out.println("*** Операцация по снятию наличных ***");
                    System.out.print("Введите сумму для вывода средств: ");
                    money = scanner.nextInt();
                    if (machine.giveMoney(money) != 0) {
                        System.out.println("---------------------------------");
                        System.out.println("Операция №" + machine.getNumberOfOperations() + " - снятие. " + "Выдана сумма: " + money + "р. " + "Ваш баланс: " + machine.getRestOfMoneyATM() + "р.");
                        System.out.println("---------------------------------");
                    } else {
                        System.out.println("---------------------------------");
                        System.out.println("Операция №" + machine.getNumberOfOperations() + " - снятие. Превышен лимит! Максимальная сумма для снятия: " + machine.getMAX_ALLOWED_AMOUNT_TO_WITHDRAW() + "р. Ваш баланс: " + machine.getRestOfMoneyATM() + "р.");
                        System.out.println("---------------------------------");
                    }
                }
                case 2 -> {
                    System.out.println("*** Операцация пополнения счета ***");
                    System.out.print("Введите сумму для пополнения баланса: ");
                    money = scanner.nextInt();
                    int remains = machine.putMoney(money);
                    if (remains == 0) {
                        System.out.println("---------------------------------");
                        System.out.println("Операция №" + machine.getNumberOfOperations() + " - внесение. " + "Ваши деньги успешно внесены. Ваш баланс: " + machine.getRestOfMoneyATM() + "р.");
                        System.out.println("---------------------------------");
                    } else {
                        System.out.println("---------------------------------");
                        System.out.println("Операция №" + machine.getNumberOfOperations() + " - внесение. Превышен лимит! Ваш баланс: " + machine.getRestOfMoneyATM() + "р. Заберите лишние деньги: " + remains + "р.");
                        System.out.println("---------------------------------");
                    }
                }
                case 3 -> {
                    System.out.println("*** Окончание работы банкомата ***");
                    System.out.println("**** До свидания! ****");
                    System.exit(0);
                }
                default -> {
                    System.out.println("Операция введена неверно, попробуйте еще раз.");
                    System.out.println("---------------------------------");
                }
            }
        }
    }
}