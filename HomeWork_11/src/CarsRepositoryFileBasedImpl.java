import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String Number = parts[0];
        String Model = parts[1];
        String Color = parts[2];
        Integer Mileage = Integer.parseInt(parts[3]);
        Integer Price = Integer.parseInt(parts[4]);
        return new Car(Number, Model, Color, Mileage, Price);
    };

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<String> carNumberIsBlackOrTheMileageIsZero(String Color, Integer Mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getMileage().equals(Mileage) || car.getColor().equals(Color))
                    .map(Car::getNumber)
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public long theNumberOfUniqueModelsInThePriceRange(Integer PriceFrom, Integer PriceTo) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() >= PriceFrom && car.getPrice() <= PriceTo)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public String theColorOfTheCarWithTheMinimumPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .orElseThrow();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public double averagePrice(String Model) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getModel().equals(Model))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}


