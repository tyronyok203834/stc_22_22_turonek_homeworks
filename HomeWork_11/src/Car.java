import java.util.Objects;

public class Car {
    private final String Number;
    private final String Model;
    private final String Color;
    private final Integer Mileage;
    private final Integer Price;

    public Car(String Number, String Model, String Color, Integer Mileage, Integer Price) {
        this.Number = Number;
        this.Model = Model;
        this.Color = Color;
        this.Mileage = Mileage;
        this.Price = Price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.Mileage, Mileage) == 0 && Objects.equals(Number, car.Number)
                && Objects.equals(Model, car.Model) && Objects.equals(Color, car.Color)
                && Objects.equals(Price, car.Price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Number, Model, Color, Mileage, Price);
    }

    public String getNumber() {
        return Number;
    }

    public String getModel() {
        return Model;
    }

    public String getColor() {
        return Color;
    }

    public Integer getMileage() {
        return Mileage;
    }

    public Integer getPrice() {
        return Price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carNumber='" + Number + '\'' +
                ", carModel='" + Model + '\'' +
                ", carColor='" + Color + '\'' +
                ", carMileage=" + Mileage +
                ", carCost=" + Price +
                '}';
    }
}
