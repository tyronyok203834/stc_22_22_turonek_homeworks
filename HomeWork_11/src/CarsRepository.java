import java.util.List;

public interface CarsRepository {
    List<String> carNumberIsBlackOrTheMileageIsZero(String Color, Integer Mileage);

    long theNumberOfUniqueModelsInThePriceRange(Integer PriceFrom, Integer PriceTo);

    String theColorOfTheCarWithTheMinimumPrice();

    double averagePrice(String Model);

}
