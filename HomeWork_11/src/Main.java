public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");
        System.out.println(carsRepository.carNumberIsBlackOrTheMileageIsZero("Black", 0));
        System.out.println(carsRepository.theNumberOfUniqueModelsInThePriceRange(700000, 800000));
        System.out.println(carsRepository.theColorOfTheCarWithTheMinimumPrice());
        System.out.println(carsRepository.averagePrice("Camry"));
    }
}
