import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество чисел в массиве: ");

        int number = scanner.nextInt();

        int[] a = new int[number];

        int localmin;
        int count = 0;

        for (int i = 0; i <= a.length - 1; i++) {
            System.out.print("Введите значение массива " + (i + 1) + " : ");
            a[i] = scanner.nextInt();
        }

        System.out.print("Локальные минимумы: ");

        if (a[1] > a[0] && a[0] < a[a.length - 1]) {
            localmin = a[0];
            count++;
            System.out.print(localmin + " ");
        }
        if (a[a.length - 2] > a[a.length - 1] && a[a.length - 1] < a[0]) {
            localmin = a[a.length - 1];
            count++;
            System.out.print(localmin + " ");
        }

        for (int i = 0; i < a.length - 1; i++) {
            if (i > 0 && a[i + 1] > a[i] && a[i] < a[i - 1]) {
                localmin = a[i];
                count++;
                System.out.print(localmin + " ");
            }
        }

        if (count == 0) {
            System.out.println("\nНет локальных минимумов!");
        } else {
            System.out.println("\nКоличество локальных минимумов: " + count);
        }
    }
}


