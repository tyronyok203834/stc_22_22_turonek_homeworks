public class Main {
    public static void main(String[] args) {

        ArrayTask sumArrayTask = (array, from, to) -> {
            int sum = 0;
            for(int i = from; i < to; i++){
                sum += array[i];
            }
            return sum;
        };

        ArrayTask maxSumArrayTask = (array, from, to) -> {
            int max = 0;
            int current;
            int sum = 0;
            for(int i = from; i < to; i++) {
                if(array[i] > max) {
                    max = array[i];
                }
            }
            current = max;
            while (current > 0) {
                sum += current % 10;
                current /= 10;
            }
            return sum;
        };

        int[] array1 = {10, 57, 81, 34, 100, 25, 18};
        int from1 = 2;
        int to1 = 5;

        ArrayTasksResolver.resolveTask(array1, sumArrayTask, from1, to1);

        int[] array2 = {12, 62, 4, 2, 100, 40, 56};
        int from2 = 0;
        int to2 = 4;

        ArrayTasksResolver.resolveTask(array2, maxSumArrayTask, from2, to2);
    }
}