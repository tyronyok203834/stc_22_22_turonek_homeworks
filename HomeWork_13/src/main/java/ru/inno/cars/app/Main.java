package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"-action"})
    String action;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Main main = new Main();

        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(new InputStreamReader
                    (Objects.requireNonNull(Main.class.getResourceAsStream("/db.properties")))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);
        if (main.action.equals("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        } else if (main.action.equals("write")) {
            while (true) {
                System.out.println("Введите модель");
                String model = scanner.nextLine();
                System.out.println("Введите цвет");
                String color = scanner.nextLine();
                System.out.println("Введите номер");
                String number = scanner.nextLine();

                Car car = Car.builder()
                        .carModel(model)
                        .carColor(color)
                        .carNumber(number)
                        .build();
                carsRepository.save(car);
                String field = scanner.nextLine();
                if (field.equalsIgnoreCase("exit") || field.equalsIgnoreCase("-1")) {
                    return;
                }
            }
        }
    }
}