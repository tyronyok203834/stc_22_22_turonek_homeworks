drop table if exists trip;
drop table if exists car;
drop table if exists client;

create table client(
    id bigserial primary key,
    fist_name char(20),
    last_name char(20),
    phone_number varchar(20),
    experience integer,
    age integer check (age >= 0 and age <= 120),
    driver_license bool,
    driving_license_category char(20),
    rating integer check (rating >= 0 and rating <= 5)
);

create table car(
    id bigserial primary key,
    car_id integer not null,
    car_model char(20) not null,
    car_color char(20) not null,
    car_number char(20) not null,
    foreign key (car_id) references client(id)
);

create table trip(
    driver_id bigint,
    car_id bigint,
    trip_date date,
    trip_time time,
    foreign key (driver_id) references client(id),
    foreign key (car_id) references client(id)
);