insert into client (fist_name, last_name, phone_number, experience, age, driver_license, driving_license_category,
                    rating)
values ('Александр', 'Туронек', 79065555555, 2, 31, true, 'B', 5),
       ('Егор', 'Тимашев', 79128451576, 15, 47, true, 'C', 3),
       ('Таисия', 'Сагадиева', 79051796695, 5, 21, false, 'B', 1),
       ('Вячеслав', 'Марьин', 79267723327, 20, 55, true, 'C', 5),
       ('Степан', 'Шерков', 79096279891, 14, 28, true, 'B', 3);

insert into car (car_id, car_model, car_color, car_number)
values (1, 'Land Rover', 'Beige', 'Х127КВ'),
       (2, 'Aston Martin', 'Orchid', 'Е590ОК'),
       (3, 'Rolls-Royce', 'Red', 'В711РО'),
       (4, 'Kia', 'Blue', 'А739КР'),
       (5, 'Mitsubishi', 'Coral', 'М070ВО');

insert into trip (driver_id, car_id, trip_date, trip_time)
values (1, 1, '2023-07-26', '03:42:52'),
       (2, 5, '2022-08-22', '22:03:44'),
       (3, 4, '2023-05-16', '04:59:42'),
       (4, 5, '2023-10-31', '01:54:40'),
       (5, 1, '2020-04-21', '18:19:09');