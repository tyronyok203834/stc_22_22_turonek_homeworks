import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] array = line.split(" ");
        Map<String, Integer> Map = new HashMap<>();
        int count = 0;
        for (String word : array) {
            if (Map.containsKey(word)) {
                Map.put(word, Map.get(word) + 1);
            } else {
                Map.put(word, 1);
            }
        }
        for (String word : Map.keySet()) {
            count++;
            if (Map.get(word) > count) {
                System.out.println(word + " " + Map.get(word));
            }
        }
    }
}